import React, { Component } from "react";
//import ReactDOM from "react-dom";
import * as THREE from "three";
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls"

class App extends Component {
      componentDidMount() {

        let scene, sphere, camera, renderer, controls

        const init = () => {
          scene = new THREE.Scene();
  
          camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
          camera.position.z = 50
          
          renderer = new THREE.WebGLRenderer({antialias: true});
          renderer.setSize(window.innerWidth, window.innerHeight);
          renderer.setPixelRatio(window.devicePixelRatio)
          renderer.setClearColor(0xc553c1)
          document.body.appendChild(renderer.domElement);
  
          let axesHelper = new THREE.AxesHelper()
          scene.add(axesHelper)
          axesHelper.position.y = 5
          
          controls = new OrbitControls(camera, renderer.domElement)
  
          let light = new THREE.DirectionalLight(0xffffff)
          light.position.set(0, 10, 1)
          scene.add(light)
  
          let hemi = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.5)
          hemi.position.set(0, 0, 5)
          scene.add(hemi)
          
          let geometry = new THREE.SphereGeometry(25, 32, 32);
          const texture = new THREE.TextureLoader().load('https://res.cloudinary.com/leonemil/image/upload/v1610828326/Demos/textura-esfera_h7vnrs.jpg');
          let material = new THREE.MeshBasicMaterial({ map: texture });
          sphere = new THREE.Mesh(geometry, material);
          scene.add(sphere);
          //camera.lookAt(0, 0, 0)
          
        }
        
        const animate = () => {
          requestAnimationFrame(animate);
          
          renderer.render(scene, camera);
          controls.update()

          //console.log(sphere)
          sphere.addEventListener("click", () => {
            console.log("click")
          })
        };
        
        
        window.addEventListener("resize", () => {
          renderer.setSize(window.innerWidth, window.innerHeight);
          renderer.setPixelRatio(window.devicePixelRatio)
          renderer.render(scene, camera)
        })
        
        init()
        animate();
        
      }

  render() {
    return (
      <div />
    )
  }
}

//const rootElement = document.getElementById("root");
//ReactDOM.render(<App />, rootElement);

export default App;
